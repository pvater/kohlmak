Config files for my custom keyboard layout "Kohlmak", usage with either xmodmap or xkb for X, or loadkeys for the console.

When i learned touchtyping, i noticed the (obvious) problems Qwerty has, mainly in terms of ergonomics.
Naturally i stumbled upon Dvorak and Colemak, and liked Colemaks philosophy of prioritizing the homerow.
Then I found Workman, which improved on some issues Colemak has.

I mostly agree with the arguments, but i didn't like that it broke Vim movement keys (HJKL).
This isn't only relevant for Vim itself, but other applications that use Vim-like movement, such as window manager (xmonad, i3, dwm, ...), terminal multiplexer (tmux or dvtm), filemanager (ranger, vifm, ...), and others.
So I moved HJKL to places that somewhat make sense for usage as movement keys, but still mostly adhered to the ideas of the Workman layout.

I don't want to make going back to Qwerty too hard (necessary when i need to type something on a collegues computer, for example).
That's why i keep special characters in their original positions; and I also swapped some more keys back to, or closer to, their original Qwerty positions, because their remapping in Workman seemed unnecessary to me.
Lastly I moved A to the right hand because i figured it should be good to have all vowels on one hand (this is probably unnecessary; at the time i had the vague idea of creating umlauts with some sort of modifier combination like Win+Ctrl, which on some keyboards might only exist on the left hand; now i think it is better to use a compose key), and swapped Q-Y and C-V for better ergonomics.

What i ended up with is this monstrosity:

 ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
 │ ~   │ !   │ @   │ #   │ $   │ %   │ ^   │ &   │ *   │ (   │ )   │ _   │ +   ┃Backspace┃
 │ `   │ 1   │ 2   │ 3   │ 4   │ 5   │ 6   │ 7   │ 8   │ 9   │ 0   │ -   │ =   ┃ ⌫       ┃
 ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┳━━━┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
 ┃Tab    ┃ Y   │ W   │ R   │ F   │ P   ┃ Q   │ M   │ U   │ O   │ :   │ {   │ }   ┃ ⏎     ┃
 ┃ ↹     ┃ y   │ w   │ r   │ f   │ p   ┃ q   │ m   │ u   │ o   │ ;   │ [   │ ]   ┃ Enter ┃
 ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
 ┃Escape  ┃ D   │ S   │ N   │ T   │ G   ┃ K   │ H   │ E   │ A   │ I   │ "   │ |   ┃      ┃
 ┃        ┃ d   │ s   │ n   │ t   │ g   ┃ k   │ h   │ e   │ a   │ i   │ '   │ \   ┃      ┃
 ┣━━━━━━┳━┹───┬─┴───┬─┴───┬─┴───┬─┴───┬─┺━━━┱─┴───┬─┴───┬─┴───┬─┴───┬─┴───┲━┷━━━━━┻━━━━━━┫
 ┃Shift ┃     │ Z   │ X   │ V   │ C   │ B   ┃ J   │ L   │ <   │ >   │ ?   ┃Shift         ┃
 ┃ ⇧    ┃     │ z   │ x   │ v   │ c   │ b   ┃ j   │ l   │ ,   │ .   │ /   ┃ ⇧            ┃
 ┣━━━━━━┻┳━━━━┷━━┳━━┷━━━━┱┴─────┴─────┴─────┻─────┴─────┴────┲┷━━━━━╈━━━━━┻┳━━━━━━┳━━━━━━┫
 ┃Ctrl   ┃ Win   ┃Alt    ┃ ␣ Space                           ┃Alt   ┃Comp. ┃ Ctrl ┃      ┃
 ┃       ┃       ┃       ┃                                   ┃      ┃      ┃      ┃      ┃
 ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━┻━━━━━━┻━━━━━━┻━━━━━━┛

The following are personal preferences which are not strictly part of the layout, but may or may not be implemented in these here files:
As hinted at above, I disagree with the notion of having to press a modifier and the modified key with the same hand.
That's why i map AltGr to Alt when applicable, and simply map Left/Right Ctrl/Alt to just Ctrl/Alt, respectively, because i don't see a point in distinguishing.
Additionally I like having Capslock mapped to Escape (very useful for Vim), and adding a composition key (mostly for umlauts).
setxkbmap has an option to simulate capslock by pressing both Shift's simultaneously, which i like and occasionally use.

I have been using this layout for years now, writing my bachelors and masters thesis as well as various papers/articles, and probably tens of thousands of lines of code in various programming languages, and never felt any strain close to what i felt when typing in Qwerty, even when typing much and fast.
When creating this layout, I also did some statistics similar to those done by the Workman creator, with both english and german texts, to convince myself that this is indeed a good layout.
Of course i didn't save any of that.
However it's still very simple to for example use this "Keyboard Layout Analyzer" tool (https://patorjk.com/keyboard-layout-analyzer/#/main) to confirm that (file to import Kohlmak is included in this repository).
